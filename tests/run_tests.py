
"""
Run all default provision tests from mpikat on a specified provision / invenory combination
"""
import asyncio
import argparse
import os
import datetime
import json
from mpikat.utils.testing.provision_tests import Stage, run_tests, TerminalOutput, load_inventory, HTMLOutput, MultiOutput
from mpikat.utils.testing.provision_tests.registry import instance

parser = argparse.ArgumentParser(description=__doc__)
parser.add_argument('-i', '--inventory', help='Inventory to use', default='effelsberg_devel')
parser.add_argument('-o', '--output', help='Default output directory', default='/beegfsEDD/edd_test_results')
# parser.add_argument('-c', '--configure', help='Parameters passed to the configure stage, must be json encode able', default='{}')
# parser.add_argument('-m', '--measurement-prepare', help='Parameters passed to the measurement-prepare stage, must be json encode able', default='{}')
parser.add_argument('provisions', nargs='*', help='Provisions to be used.')

args = parser.parse_args()

try:
    import dbbc.provision_tests
except ImportError:
    print("Couldn't import dbbc")

provisions = set([os.path.splitext(f)[0].replace("provison_descriptions/", "") for f in args.provisions])

print(f'Testing {len(provisions)} provisions')

now = datetime.datetime.now().isoformat()

# configure = json.loads(args.configure)
# measurement_prepare = json.loads(args.measurement_prepare)

for i, p in enumerate(provisions):
    instance().clear()
    output_prefix = os.path.join(args.output, args.inventory, *now.split('T')[0].split('-'))

    os.umask(0)
    os.makedirs(output_prefix, exist_ok=True, mode=0o777)

    print(f"Running on {i+1} / {len(provisions)}: {p}")
    stage_parameters = {Stage.provision: p,
                        # Stage.configure: configure,
                        # Stage.measurement_prepare: measurement_prepare,
                        Stage.measurement_stop: 1} # Get created files as response from the MasterController

    # Define outputs
    html = HTMLOutput()
    output = MultiOutput(TerminalOutput(), html )
    inventory_data=load_inventory(args.inventory)
    # Run tests
    res = asyncio.run(run_tests(output=output,
                                inventory_data=load_inventory(args.inventory),
                                stage_parameters=stage_parameters))

    # Save HTML Report
    ofile = os.path.join(output_prefix, f'EDD_TEST_REPORT_{now}_{os.path.basename(p)}.html')
    print(f'Writing output to {ofile}\n')
    with open(ofile, 'w') as f:
        f.write(html.to_string())
    os.chmod(ofile, 0o744)