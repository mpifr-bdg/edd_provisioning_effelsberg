import unittest
import unittest.mock
import git
import tempfile
import os
import io
import datetime

import deploy_tool.app as deploy_tool


class TestTempRepository(unittest.TestCase):
    def setUp(self):
        # Set up a origin repository
        self.tmpdir = tempfile.TemporaryDirectory()

        self.origin_repo = git.Repo.init(self.tmpdir.name)
        foo_file = os.path.join(self.tmpdir.name, 'requirements.yml')
        with open(foo_file, 'w') as f:
            f.write('---')
        self.origin_repo.index.add(foo_file)
        self.origin_repo.index.commit('Added foo file')

    def tearDown(self):
        self.tmpdir.cleanup()

    def test_init(self):
        # After init, the repository should be cloned
        repo = deploy_tool.TempRepository(self.tmpdir.name)
        repo.init()

        with open(os.path.join(repo._tmpdir.name, 'requirements.yml')) as f:
            data = f.read()
        self.assertEqual(data, '---')

    def test_next_version(self):
        # If no version exists, a well defined tag should be created
        repo = deploy_tool.TempRepository(self.tmpdir.name)
        repo.init()

        v = repo.get_next_version(datetime.datetime(2022, 3, 24))
        self.assertEqual(v, "220324.0")
        repo.repo.create_tag(v)

        v = repo.get_next_version(datetime.datetime(2022, 3, 24))
        self.assertEqual(v, "220324.1")

        # There should be no tag for today, so there should be one ending with
        # 0 next
        vt = repo.get_next_version()
        self.assertTrue(vt.endswith('.0'))
        repo.repo.create_tag(vt)
        vt = repo.get_next_version()
        self.assertTrue(vt.endswith('.1'))

    def test_release(self):
        # After release, the new tag should exist in the origin repository
        repo = deploy_tool.TempRepository(self.tmpdir.name)
        repo.init()

        nt = repo.get_next_version()

        repo.release()

        self.assertIn(nt, [t.name for t in self.origin_repo.tags])


    def test_list_version(self):
        VL = deploy_tool.get_version_list(self.tmpdir.name)
        self.assertListEqual(VL, [])

        self.origin_repo.create_tag('foo')
        VL = deploy_tool.get_version_list(self.tmpdir.name)
        self.assertListEqual(VL, ['foo'])


    def test_cleanenvironment_passphrase(self):
        pwd = 'scientiavinceretenebras'
        # password should be queried and saved to a file in the tem directory

        inventory = self.tmpdir.name
        inventory_settings = {}
        version = '230201.0'

        with unittest.mock.patch('getpass.getpass', side_effect=[pwd]) as stdin, unittest.mock.patch('subprocess.run') as sub, deploy_tool.CleanEnvironment(inventory, inventory_settings, version) as ctx:
            with open(os.path.join(ctx.build_directory, 'vault_pwd')) as f:
                d = f.read()
                self.assertEqual(pwd, d)



if __name__ == "__main__":
    unittest.main()
