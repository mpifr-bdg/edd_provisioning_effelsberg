---
# Repository Settings
#####################

# This version tag is used to tag all containers build + run with the
# provisioning systems. The default "latest" is set in the common role and
# overriden here for the production environment.

version_tag: "241120.0"

# Tag name used for loading pipelines in teh amster controller. All created
# products will be tagegd with version and deploy tag.Thus, changing a single
# pipeline in production becomes easy:  just tag the container
deploy_tag: "production"

# These settings specify the repositories used to build the EDD
# In particular useful to be tweaked in a local custom mysite_dev inventory to develop  the provisioning system.

# Repository and branch used to check out mpikat
mpikat_repository: "https://gitlab.mpcdf.mpg.de/mpifr-bdg/mpikat.git"
mpikat_branch: "240906.0"

# Repository and branch of the provisiioing system (roles and provision descriptions) used by the master controller
provision_repository: "https://gitlab.mpcdf.mpg.de/mpifr-bdg/edd_provisioning_effelsberg.git"
provision_branch: "241120.0"

# Inventory file / directory in this repository to be used.
# ToDo: replace with look up of the current inventory
edd_inventory_folder: "effelsberg_production"

# Data path for the docker registry
docker_registry_data_path: "/beegfsEDD/edd_docker_registry"

# Data path for the influx db database
influx_db_path: "/beegfsEDD/edd_influx_data"
imflux_memory_limit: 8g

influx_retention: 30w
loki_retention: 30w


# Architecture for gcc optimizations
gcc_architecture: "broadwell"   # to use same container on pacifixes as on edds

# Tweak to use the correct python version depending on the used ansible version.
ansible_python_interpreter: auto_legacy_silent

# use sudo
#ansible_become: no

# If true, packetizers will be modified during roll-out
enable_packetizer_management: True

# In production, all versions are specified, so we can safely use docker cache
use_docker_cache: True

# Network configuration
#######################
#
# Parameters of the network configuration of the site

# The subnet from which the katcp server accept control connections
edd_subnet: "0.0.0.0"

# Subnet used for highspeed data connections
high_speed_data_subnet: "10.10.0.0"
high_speed_data_subnetmask: "255.255.0.0"

# Address range used by the master cotnroller to automatically assign data streams
multicast_network: '239.64.0.0/16'


# Port used for the ssh conenctions of the master controller tot he ansible interface
edd_ansible_port: 2222

enable_dhcpd: True

# Variables for Effelsberg specific services
##############################################
#
# These variables are only used for the effelsberg setup.
log_server: "effmonitor1:12204"

fits_interface_port: 5002
effelsberg_scpi_port: 5025

DEBMIRROR: http://debcache.mpifr-bonn.mpg.de/ubuntu/

# File System Settings
#########################
#
# Settings of the shared file system / common file system layout of the nodes.

# Data base path to be used for pipelines. The pipelines will create
# data_base_path/container_name as directory and see this internally as /mnt
# This can be overriden on per role basis by setting data_path variable
# for the role
data_base_path: "/beegfsEDD/EDD_pipeline_data/production"

# base path as used for additional data definitions
# ToDo: Move to role default variables
base_path: "{{ playbook_dir }}"



# Service variables
#####################
#
# These variables are used to specify the host and port on which a specific
# service is running. Typically, the first available host in a service group is
# used. Configuring the same role on multiple service machines thus automatically
# provides a simple redundancy automatically.

redis_host: "{{ groups['redis'][0] }}"
redis_port: 6379

docker_registry: "{{ groups['registry'][0] }}"
docker_registry_port: "5000"

master_controller_port: 7147

interface_host:  "{{ groups['interface'][0] }}"

influx_host: "{{ groups['influx'][0] }}"
influxdb_port: "8086"

grafana_host: "{{ groups['grafana'][0] }}"
grafana_port: "3000"
#grafana_anonymous_role: Viewer

loki_host: "{{ groups['loki'][0] }}"
loki_port: 3100

# Non-PC Devices
################
#
# Any device which are present in th ansible configuration, in particular
# devices with entwork interface, but to which ansible cannot log on to control
# them.

npc_devices:
  skarab02:
    interfaces:
      0:
        mac: 06:50:02:09:2b:01
        ip: 10.10.1.61
      1:
        mac: 06:50:02:09:2b:02
        ip: 10.10.1.62
    control_port: 7147

  alveo_00: #fpga001 -> to be moved to alveo_host group
    interfaces:
      0:
        mac: 00:0A:35:06:9E:EC
        ip: 10.10.1.34
      1:
        mac: 00:0A:35:06:9E:ED
        ip: 10.10.1.35
    device: 0
    control_port: 7180

  alveo_01: #fpga000 -> to be moved to alveo_host group
    interfaces:
      0:
        mac: 00:0A:35:06:9E:EA
        ip: 10.10.1.32
      1:
        mac: 00:0A:35:06:9E:EB
        ip: 10.10.1.33
    device: 0
    control_port: 7180


