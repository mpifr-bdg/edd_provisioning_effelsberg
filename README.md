# Effelsberg provision repository

This repository contains the configuration and provision descriptions specific
to the the EFFELSBERG deployment of the EDD.


## Build of the EDD
There should be no need to build the edd as contaienrs are build by the cicd
system. If a manual build is needed, the edd can be build by

```
$ ansible-playbook -i effelsberg_devel effelsberg_config.yml --tags=build --ask-vault-password
```

If neede the, locally buildcontainers can be published in the mpcdf registry
using

```
$ ansible-playbook -i effelsberg_devel effelsberg_config.yml --tags=pushremote --ask-vault-password
```

## Update the local containers
If not build manually, the default way to get the altest version is to just
pull the contaienrs from the mpcdf  docker registry:

```
$ ansible-playbook -i effelsberg_devel efelsberg_config.yml --tags=pullremote
```

## Deployment

To deploy the EDD, use:

```
$ ansible-playbook -i effelsberg_devel effelsberg_config.yml --ask-vault-password
```


## Retagging only
To retag the current 'latest' containers as production verions, you can use

`$ ansible-playbook -i effelsberg_production effelsberg_config.yml --tags retag_latest --ask-vault-password`

NOTE THAT AFTER DEPLOYING, THE REPOSITORY INSIDE THE MASTER CONTROLLER NEEDS TO
BE UPDATED ALSO IF A PRODUCTION VERSION IS CREATED LIKE THAT.

However, it is probably best to always quick-build the master
controller conventionally after retagging.




